--Migracion
CREATE TABLE dbo.temp (
       formatojson nvarchar(max)
       );
--1. Empleado y Servicio
--1.1 se construye query
SELECT * FROM dbo.Empleado EM
LEFT JOIN dbo.Servicio SE ON SE.CodS=EM.CodS
--1.2 se analiza un registro y se transforma a Json
SELECT * FROM dbo.Empleado EM
LEFT JOIN dbo.Servicio SE ON SE.CodS=EM.CodS
WHERE em.NumReg=1
FOR JSON AUTO
--1.3 Finalmente se consturye el json para cada uno y se guardara en una tabla temporal para crear los documentos
DECLARE @json VARCHAR(MAX)
DECLARE @REG INT

DECLARE VARIABLE CURSOR  
    FOR SELECT NumReg FROM dbo.Empleado
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM dbo.Empleado EM
LEFT JOIN dbo.Servicio SE ON SE.CodS=EM.CodS
WHERE EM.NumReg=@REG
FOR JSON AUTO)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

CLOSE VARIABLE  
DEALLOCATE VARIABLE 



--2.Habitacion Precio Factura
--2.1 query
SELECT Habitacion.Numero,superficie,bar,terraza,puedesupletoria,Habitacion.tipo,precio,factura.CodigoF,factura.Entrada,factura.Salida,factura.DNI,factura.supletoria,factura.Forma,factura.Total FROM dbo.Habitacion 
 INNER JOIN dbo.Precio precio ON precio.Tipo = Habitacion.Tipo
 LEFT JOIN dbo.Factura factura ON  factura.Numero = Habitacion.Numero
--2.1 json
SELECT * FROM (
 SELECT Habitacion.Numero,superficie,bar,terraza,puedesupletoria,Habitacion.tipo,precio,factura.CodigoF,factura.Entrada,factura.Salida,factura.DNI,factura.supletoria,factura.Forma,factura.Total FROM dbo.Habitacion 
 INNER JOIN dbo.Precio precio ON precio.Tipo = Habitacion.Tipo
 LEFT JOIN dbo.Factura factura ON  factura.Numero = Habitacion.Numero
 WHERE CodigoF=1) AS t2
 FOR JSON AUTO
 --2.2 json pa todos

 
DECLARE VARIABLE CURSOR  
    FOR SELECT CodigoF FROM dbo.Factura
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM (
 SELECT Habitacion.Numero,superficie,bar,terraza,puedesupletoria,Habitacion.tipo,precio,factura.CodigoF,factura.Entrada,factura.Salida,factura.DNI,factura.supletoria,factura.Forma,factura.Total FROM dbo.Habitacion 
 INNER JOIN dbo.Precio precio ON precio.Tipo = Habitacion.Tipo
 LEFT JOIN dbo.Factura factura ON  factura.Numero = Habitacion.Numero
 WHERE CodigoF=@REG) AS t2
 FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

CLOSE VARIABLE  
DEALLOCATE VARIABLE 


--3. Cliente con facturas para saber sus estancias
--3.1 query
SELECT * FROM dbo.Cliente cliente
INNER JOIN dbo.Factura fact ON fact.DNI = cliente.DNI
--3.2 json
SELECT * FROM dbo.Cliente cliente
INNER JOIN dbo.Factura fact ON fact.DNI = cliente.DNI
WHERE fact.CodigoF=1
FOR JSON AUTO
--3.3 json pa todo

 
DECLARE VARIABLE CURSOR  
    FOR SELECT CodigoF FROM dbo.Factura
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM dbo.Cliente cliente
INNER JOIN dbo.Factura fact ON fact.DNI = cliente.DNI
WHERE fact.CodigoF=@REG
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

CLOSE VARIABLE  
DEALLOCATE VARIABLE 


--4 clientes y factura
--4.1 query
SELECT * FROM dbo.Cliente cliente
INNER JOIN dbo.Factura factura ON factura.DNI = cliente.DNI
--4.2 json
SELECT * FROM dbo.Cliente cliente
INNER JOIN dbo.Factura factura ON factura.DNI = cliente.DNI
WHERE factura.CodigoF=1
FOR JSON AUTO
--4.3 para todos

DECLARE VARIABLE CURSOR  
    FOR SELECT CodigoF FROM dbo.Factura
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM dbo.Cliente cliente
INNER JOIN dbo.Factura factura ON factura.DNI = cliente.DNI
WHERE factura.CodigoF=@REG
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

CLOSE VARIABLE  
DEALLOCATE VARIABLE 

--5. limpieza
--5.1 query
SELECT ROW_NUMBER() OVER(ORDER BY Numero ASC) AS Row1,dbo.Limpieza.* INTO #temlimpieza FROM dbo.Limpieza
SELECT * FROM #temlimpieza
--5.2 json
SELECT * FROM #temlimpieza
WHERE Row1=1
FOR JSON AUTO
--5.3 para todos

DECLARE VARIABLE CURSOR  
    FOR SELECT Row1 FROM #temlimpieza
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM #temlimpieza
WHERE Row1=@REG
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

drop TABLE #temlimpieza
CLOSE VARIABLE  
DEALLOCATE VARIABLE 



--6. Usa
--6.1 query
SELECT ROW_NUMBER() OVER(ORDER BY Cods ASC) AS Row1,dbo.Usa.* INTO #temusa  FROM dbo.Usa
SELECT * FROM #temusa
--6.2 json
SELECT * FROM #temusa
WHERE Row1=1
FOR JSON AUTO
--6.3 para todos
DECLARE VARIABLE CURSOR  
    FOR SELECT Row1 FROM #temusa
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM #temusa
WHERE Row1=@REG
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

drop TABLE #temusa
CLOSE VARIABLE  
DEALLOCATE VARIABLE 



--7. Incluye
--7.1 query
SELECT ROW_NUMBER() OVER(ORDER BY Cods ASC) AS Row1,dbo.Incluye.* INTO #temincluye  FROM dbo.Incluye
SELECT * FROM #temincluye
--7.2 json
SELECT * FROM #temincluye
WHERE Row1=1
FOR JSON AUTO
--7.3 para todos
DECLARE VARIABLE CURSOR  
    FOR SELECT Row1 FROM #temincluye
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM #temincluye
WHERE Row1=@REG
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

drop TABLE #temincluye
CLOSE VARIABLE  
DEALLOCATE VARIABLE 



--8. Reserva
--8.1 query
SELECT ROW_NUMBER() OVER(ORDER BY Numero ASC) AS Row1, dbo.Reserva.* INTO #temReserva  FROM  dbo.Reserva
SELECT * FROM #temReserva
--8.2 json
SELECT * FROM #temReserva
WHERE Row1=1
FOR JSON AUTO
--8.3 para todos
DECLARE VARIABLE CURSOR  
    FOR SELECT Row1 FROM #temReserva
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT * FROM #temReserva
WHERE Row1=@REG
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

drop TABLE #temReserva
CLOSE VARIABLE  
DEALLOCATE VARIABLE



--9. Proovedor factura proveedr
--9.1 query
SELECT *   FROM  dbo.Proveedor proveedor
inner JOIN dbo.Factura_Prov facprov ON facprov.NIF = proveedor.NIF
--9.2 json
SELECT *   FROM  dbo.Proveedor proveedor
inner JOIN dbo.Factura_Prov facprov ON facprov.NIF = proveedor.NIF
WHERE facprov.CodFP=1
FOR JSON AUTO
--9.3 para todos
DECLARE VARIABLE CURSOR  
    FOR SELECT CodFP FROM dbo.Factura_Prov
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT *   FROM  dbo.Proveedor proveedor
inner JOIN dbo.Factura_Prov facprov ON facprov.NIF = proveedor.NIF
WHERE facprov.CodFP=@REG
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG 
END 

CLOSE VARIABLE  
DEALLOCATE VARIABLE

SELECT * FROM dbo.temp
--DELETE FROM dbo.temp




--10. FormaPago
--10.1 query
SELECT *   FROM  dbo.FormaPago
--9.2 json
SELECT *   FROM  dbo.FormaPago
WHERE Forma='efectivo'
FOR JSON AUTO
--9.3 para todos
DECLARE @REG2 VARCHAR(20)
DECLARE VARIABLE CURSOR  
    FOR SELECT Forma FROM dbo.FormaPago
OPEN VARIABLE
FETCH NEXT FROM VARIABLE INTO @REG2 

WHILE @@FETCH_STATUS = 0
BEGIN

--INSERT into dbo.temp
SET @json=( 
SELECT *   FROM  dbo.FormaPago
WHERE Forma=@REG2
FOR JSON AUTO
)

INSERT INTO dbo.temp
SELECT	SUBSTRING(@json,3,LEN(@json)-4) --ajuste para couchdb

FETCH NEXT FROM VARIABLE INTO @REG2 
END 

CLOSE VARIABLE  
DEALLOCATE VARIABLE

SELECT * FROM dbo.temp
